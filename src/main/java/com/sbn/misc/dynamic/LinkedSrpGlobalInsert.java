package com.sbn.misc.dynamic;

import com.sbn.utills.ConnectionManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.PreparedStatement;
import java.util.Date;
import java.util.List;

public class LinkedSrpGlobalInsert {
    public static Logger LOGGER = LogManager.getLogger();
    public static void main(String[] args) throws Exception {
        ConnectionManager cm = new ConnectionManager("mn4sg3xdbsw101.ideasstg.int", "master", "sa", "IDeaS123");
        List<String> lines = Files.readAllLines(Paths.get("C:\\Users\\idnkiw\\OneDrive - SAS\\Desktop\\HCRS_SRP_Derivation_20210427.psv"));
        String strQuery = "Insert InTo [Ratchet].[dbo].Client_Linked_SRP_Mappings (Property_Code,SRP_Code,Linked_Srp_Code,Offset_Type,Yield_As,Created_DTTM,External_System) values (?,?,?,?,?,CURRENT_TIMESTAMP,'PCRS')";
        for(int pid=22061;pid<=22080;pid++) {
            String delQuery = "Delete from [Ratchet].[dbo].Client_Linked_SRP_Mappings where Property_Code = '"+pid+"'";
            cm.Execute(delQuery);
            PreparedStatement myStatements = cm.getPreparedStatementForQuery(strQuery);
            long startTime = new Date().getTime();
            LOGGER.info("adding Linked SRPS for property:"+pid);
            int i=0;
            for (String line : lines) {
                String[] cols = line.split("\\|");
                if (i != 0) {
                    myStatements.setString(1, ""+pid);
                    for(int j=1;j<=4;j++) {
                        myStatements.setString(j+1, cols[j]);
                    }
                    myStatements.addBatch();
                }
                i++;
            }
            myStatements.executeBatch();
            long endTime = new Date().getTime();
            LOGGER.info("################ took "+(endTime-startTime)+"ms");
        }
        cm.dropConnection();
    }
}

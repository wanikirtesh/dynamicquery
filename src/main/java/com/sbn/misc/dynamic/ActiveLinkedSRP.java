package com.sbn.misc.dynamic;

import com.sbn.utills.ConnectionManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class ActiveLinkedSRP {
    static final Logger LOGGER = LogManager.getLogger();
    public static void main(String[] args) throws Exception {
        String[][] seasonArray = {{"2018-05-01","2019-05-01"},
                {"2019-05-02","2019-06-01"},
                {"2019-07-02","2019-07-15"},
                {"2019-08-02","2019-09-01"},
                {"2019-09-02","2019-10-01"},
                {"2019-10-02","2019-11-01"},
                {"2019-11-02","2019-12-01"},
                {"2019-12-02","2020-01-01"},
                {"2020-01-02","2020-02-01"},
                {"2020-02-02","2020-03-01"},
                {"2020-03-02","2020-04-01"},
                {"2020-04-02","2020-05-15"},
                {"2020-05-20","2020-06-01"},
                {"2020-06-02","2020-07-01"},
                {"2020-07-02","2020-08-01"},
                {"2020-08-02","2020-09-01"},
                {"2020-09-02","2020-10-01"},
                {"2020-10-02","2020-11-01"},
                {"2020-11-02","2021-12-01"}};
        populateActiveLinkedSRP(seasonArray);
        populateActiveDefaultSRP(seasonArray);
    }

    private static void populateActiveDefaultSRP(String[][] seasonArray) throws Exception {
        ConnectionManager conGlobal = new ConnectionManager("mn4sg3xdbsw101.ideasstg.int", "Ratchet", "sa", "IDeaS123");
        ConnectionManager conTenant = new ConnectionManager("mn4sg3xdbsw102.ideasstg.int", "Master", "sa", "IDeaS123");
        String strSQL = "Insert into [Ratchet].[dbo].[Client_Active_Srps] (Property_Code,SRP_Code,Start_Date,End_Date,External_System,Created_DTTM) values (?,?,?,?,?,CURRENT_TIMESTAMP)";
        PreparedStatement myStatements = conGlobal.getPreparedStatementForQuery(strSQL);
        long i =1;
        for(int property = 20001;property<20200;property++){
            ResultSet tenantRecord = conTenant.getRecords("Select rate_code_name  from ["+property+"].[dbo].[rate_qualified] where rate_qualified_type_id=3 and status_id=1");
            ResultSet globalRecord = conGlobal.getRecords("Select top 1 * from [Ratchet].[dbo].[Client_Linked_SRP_Mappings] where property_code = 'P"+property+"'");
            globalRecord.next();
            myStatements.setString(1,globalRecord.getString("Property_Code"));
            myStatements.setString(5,globalRecord.getString("External_System"));
            while (tenantRecord.next()){
                myStatements.setString(2,tenantRecord.getString("rate_code_name"));
                for (String[] season : seasonArray) {
                    myStatements.setString(3,season[0]);
                    myStatements.setString(4,season[1]);
                    myStatements.addBatch();
                    if(i%5000==0){
                        myStatements.executeBatch();
                        myStatements.clearBatch();
                        LOGGER.info("running Batch " + i);
                    }
                    i++;
                }
            }
        }
        myStatements.executeBatch();
        myStatements.clearBatch();
        conGlobal.dropConnection();
        conTenant.dropConnection();
    }

    private static void populateActiveLinkedSRP(String[][] seasonArray) throws Exception {
        ConnectionManager cn = new ConnectionManager("mn4sg3xdbsw101.ideasstg.int", "Ratchet", "sa", "IDeaS123");
        ResultSet records = cn.getRecords("Select * from [Ratchet].[dbo].[Client_Linked_SRP_Mappings] where property_code like 'P20%'");
        String strSQL = "Insert into [Ratchet].[dbo].[Client_Active_Srps] (Property_Code,SRP_Code,Start_Date,End_Date,External_System,Created_DTTM) values (?,?,?,?,?,CURRENT_TIMESTAMP)";
        PreparedStatement myStatements = cn.getPreparedStatementForQuery(strSQL);
        int i=1;
        while (records.next()){
            myStatements.setString(1,records.getString("Property_Code"));
            myStatements.setString(2,records.getString("SRP_Code"));
            myStatements.setString(5,records.getString("External_System"));
            for (String[] season : seasonArray) {
                myStatements.setString(3,season[0]);
                myStatements.setString(4,season[1]);
                myStatements.addBatch();
                if(i%5000==0){
                    myStatements.executeBatch();
                    myStatements.clearBatch();
                    LOGGER.info("running Batch " + i);
                }
                i++;
            }
            myStatements.executeBatch();
            myStatements.clearBatch();
        }
        myStatements.close();
        cn.dropConnection();
    }


}

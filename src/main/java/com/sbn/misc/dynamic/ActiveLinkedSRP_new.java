package com.sbn.misc.dynamic;

import com.sbn.utills.ConnectionManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ActiveLinkedSRP_new {
    static final Logger LOGGER = LogManager.getLogger();
    private static ExecutorService executor;

    public static void main(String[] args) throws Exception {
        String[][] seasonArray = { { "2018-05-01", "2019-05-01" }, { "2019-05-02", "2019-06-01" },
                { "2019-07-02", "2019-07-15" }, { "2019-08-02", "2019-09-01" }, { "2019-09-02", "2019-10-01" },
                { "2019-10-02", "2019-11-01" }, { "2019-11-02", "2019-12-01" }, { "2019-12-02", "2020-01-01" },
                { "2020-01-02", "2020-02-01" }, { "2020-02-02", "2020-03-01" }, { "2020-03-02", "2020-04-01" },
                { "2020-04-02", "2020-05-15" }, { "2020-05-20", "2020-06-01" }, { "2020-06-02", "2020-07-01" },
                { "2020-07-02", "2020-08-01" }, { "2020-08-02", "2020-09-01" }, { "2020-09-02", "2020-10-01" },
                { "2020-10-02", "2020-11-01" }, { "2020-11-02", "2021-12-01" } };
        populateActiveLinkedSRP(seasonArray);
        populateActiveDefaultSRP(seasonArray);
    }

    private static void populateActiveDefaultSRP(String[][] seasonArray) throws Exception {
        ConnectionManager conGlobal = new ConnectionManager("mn4sg3xdbsw101.ideasstg.int", "Ratchet", "sa", "IDeaS123");
        ConnectionManager conTenant = new ConnectionManager("mn4sg3xdbsw102.ideasstg.int", "Master", "sa", "IDeaS123");
        String strSQL = "Insert into [Ratchet].[dbo].[Client_Active_Srps] (Property_Code,SRP_Code,Start_Date,End_Date,External_System,Created_DTTM) values (?,?,?,?,?,CURRENT_TIMESTAMP)";
        List<String[]> entries = new ArrayList<String[]>();
        LOGGER.info("********** Adding Entries **********");
        for (int property = 20001; property < 20200; property++) {
            ResultSet tenantRecord = conTenant.getRecords("Select rate_code_name  from [" + property
                    + "].[dbo].[rate_qualified] where rate_qualified_type_id=3 and status_id=1");
            ResultSet globalRecord = conGlobal.getRecords(
                    "Select top 1 * from [Ratchet].[dbo].[Client_Linked_SRP_Mappings] where property_code = 'P"
                            + property + "'");
            globalRecord.next();
            while (tenantRecord.next()) {
                for (String[] season : seasonArray) {
                    entries.add(new String[] { globalRecord.getString("Property_Code"),
                            tenantRecord.getString("rate_code_name"), season[0], season[1],
                            globalRecord.getString("External_System") });
                }
            }
        }
        conGlobal.dropConnection();
        conTenant.dropConnection();
        insertBulkThreaded(strSQL, entries);
    }

    private static void populateActiveLinkedSRP(String[][] seasonArray) throws Exception {
        ConnectionManager cn = new ConnectionManager("mn4sg3xdbsw101.ideasstg.int", "Ratchet", "sa", "IDeaS123");
        ResultSet records = cn.getRecords(
                "Select * from [Ratchet].[dbo].[Client_Linked_SRP_Mappings] where property_code like 'P20%'");
        String strSQL = "Insert into [Ratchet].[dbo].[Client_Active_Srps] (Property_Code,SRP_Code,Start_Date,End_Date,External_System,Created_DTTM) values (?,?,?,?,?,CURRENT_TIMESTAMP)";
        List<String[]> entries = new ArrayList<String[]>();
        LOGGER.info("********** Adding Entries **********");
        while (records.next()) {
            for (String[] season : seasonArray) {
                entries.add(new String[] { records.getString("Property_Code"), records.getString("SRP_Code"), season[0],
                        season[1], records.getString("External_System") });
            }
        }
        cn.dropConnection();
        insertBulkThreaded(strSQL, entries);
    }

    public static void insertBulkThreaded(String strSQL, List<String[]> entries) {
        LOGGER.info("********** Executing Entries **********");
        int threads = 10;
        int totalEntries = entries.size();
        int batchSize = totalEntries / threads;
        executor = Executors.newFixedThreadPool(threads);
        LOGGER.info("total records " + totalEntries + " and batch size will be " + batchSize);
        for (int i = 0; i < 10; i++) {
            LOGGER.info(entries.subList(i * batchSize, i < 9 ? (i + 1) * batchSize : entries.size()).size());
            Runnable thread = new QueryBatchRunner(
                    entries.subList(i * batchSize, i < 9 ? (i + 1) * batchSize : entries.size()), strSQL,
                    "mn4sg3xdbsw101.ideasstg.int");
            executor.execute(thread);
        }
        executor.shutdown();
        while (!executor.isTerminated()) {

        }
        LOGGER.info("\nFinished all threads");
    }
}

package com.sbn.misc.dynamic;

import com.sbn.utills.ConnectionManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.PreparedStatement;
import java.util.List;

public class QueryBatchRunner implements Runnable {
    private static final Logger LOGGER = LogManager.getLogger();
    private final List<String[]> entries;
    private final String strQuery;
    private final String server;
    private final int batchSize = 5000;

    public QueryBatchRunner(List<String[]> entries, String strQuery, String server) {
        this.entries = entries;
        this.strQuery = strQuery;
        this.server = server;
    }

    @Override
    public void run() {
        ConnectionManager cm = null;
        try {
            int i = 1;
            cm = new ConnectionManager(server, "Ratchet", "sa", "IDeaS123");
            PreparedStatement myStatements = cm.getPreparedStatementForQuery(strQuery);
            int batchNumber = 1;
            for (String[] entry : entries) {
                int j = 1;
                for (String s : entry) {
                    myStatements.setString(j++, s);
                }
                myStatements.addBatch();
                if (i % batchSize == 0) {
                    myStatements.executeBatch();
                    myStatements.clearBatch();
                    LOGGER.info("completed Batch " + batchNumber++);
                }
                i++;
            }
            LOGGER.info("completed Batch " + batchNumber++);
            myStatements.executeBatch();
            myStatements.clearBatch();
            myStatements.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        cm.dropConnection();
    }
}

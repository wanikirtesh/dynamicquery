package com.sbn.misc.dynamic;

import com.sbn.utills.MySQLConnectionManager;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.PreparedStatement;
import java.util.List;

public class DumpSRP {
    public static void main(String[] args) throws Exception {
        dumpActiveSRP();
        dumpSRP();

    }

    private static void dumpSRP() throws Exception {
        MySQLConnectionManager cm = new MySQLConnectionManager("localhost", "mytest", "root", "admin");
        List<String> lines = Files.readAllLines(Paths
                .get("C:\\Users\\idnkiw\\OneDrive - SAS\\Desktop\\newprop\\ATLES\\HCRS_SRP_Derivation_20210525.psv"));
        String strQuery = "Insert InTo srps (pcode,srp) values (?,?)";
        PreparedStatement myStatements = cm.getPreparedStatementForQuery(strQuery);
        int i = 1;
        int j = 1;
        for (String line : lines) {
            String cols[] = line.split("\\|");
            myStatements.setString(1, cols[0]);
            myStatements.setString(2, cols[1]);
            myStatements.addBatch();

            if (i % 10000 == 0) {
                myStatements.executeBatch();
                myStatements.clearBatch();
                System.out.println("running batch" + j++);
            }
            i++;
        }
        myStatements.executeBatch();

        cm.dropConnection();
    }

    private static void dumpActiveSRP() throws Exception {
        MySQLConnectionManager cm = new MySQLConnectionManager("localhost", "mytest", "root", "admin");
        List<String> lines = Files.readAllLines(
                Paths.get("C:\\Users\\idnkiw\\OneDrive - SAS\\Desktop\\Active_SRP\\HCRS_ACTIVE_SRP_20210504.psv"));
        String strQuery = "Insert InTo activesrp (property,srp,startDate,endDate) values (?,?,?,?)";
        PreparedStatement myStatements = cm.getPreparedStatementForQuery(strQuery);
        int i = 1;
        int j = 1;
        for (String line : lines) {
            String cols[] = line.split("\\|");
            myStatements.setString(1, cols[0]);
            myStatements.setString(2, cols[1]);
            myStatements.setString(3, cols[2]);
            myStatements.setString(4, cols[3]);
            myStatements.addBatch();
            if (i % 5000 == 0) {
                myStatements.executeBatch();
                myStatements.clearBatch();
                System.out.println("running batch" + j++);
            }
            i++;
        }
        myStatements.executeBatch();
        cm.dropConnection();
    }
}

package com.sbn.misc.dynamic;

import com.sbn.utills.InterNetHelper;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import java.io.IOException;
import java.util.List;

public class FileDownloader {
    public static void main(String[] args) throws IOException, InterruptedException {
        System.setProperty("webdriver.chrome.driver", "c:/driver/chromedriver.exe");
        ChromeOptions co = new ChromeOptions();
        co.setHeadless(true);
        WebDriver driver = new ChromeDriver(co);
        String url = "http://aspscripts.ideasprod.int/rateshopping_info/glb2201ext/extracts/rss-1/Hilton-KOAHW/";
        String path = "c:/rss/";
        driver.get(url);
        List<WebElement> links = driver.findElements(By.xpath(
                "//a/following-sibling::text()[contains(.,'2021-05-12')]/preceding-sibling::a[contains(@href,'Hilton')]"));
        for (WebElement link : links) {
            String fName = link.getAttribute("href");
            System.out.println("downloading " + fName);
            InterNetHelper.downloadUsingNIO(fName, path + fName.substring(fName.lastIndexOf("/") + 1));
        }
    }

}

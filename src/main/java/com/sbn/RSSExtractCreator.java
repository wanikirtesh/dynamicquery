package com.sbn;

import com.sbn.utills.FileHelper;
import org.apache.logging.log4j.*;
import java.io.File;
import java.io.IOException;
import java.nio.file.*;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Stream;

public class RSSExtractCreator {
    private static final Logger LOGGER = LogManager.getLogger();

    public static void main(String[] args) throws IOException, InterruptedException {
        // Path source = Paths.get(args[0]);
        Path source = Paths.get("c:\\rss");
        Path working = Paths.get(source + File.separator + "temp");
        // Path destination = Paths.get(args[1]);
        Path destination = Paths.get("c:\\rss_new");
        cleanDirectory(destination);
        if (!Files.isDirectory(working)) {
            Files.createDirectory(working);
        }
        // int stp = Integer.parseInt(args[2]);
        int stp = 20100;
        // int nop = Integer.parseInt(args[3]);
        int nop = 1;
        Stream<Path> paths = Files.list(source);
        for (Path file : paths.toArray(Path[]::new)) {
            if (!Files.isDirectory(file)) {
                cleanDirectory(working);
                FileHelper.unzipFile(file, working);
                for (int i = stp; i < stp + nop; i++) {
                    String prcode = "P" + i;
                    LOGGER.info("working for " + file + " " + prcode);
                    Path changedFile = updateFile(Paths.get(
                            working.toString() + File.separator + file.getFileName().toString().replace("zip", "xml")),
                            prcode);
                    FileHelper.zipSingleFile(changedFile, destination);
                }
            }
        }
        paths.close();
    }

    private static void cleanDirectory(Path working) throws IOException {
        if (Files.exists(working) && Files.isDirectory(working)) {
            for (Path file : Files.list(working).toArray(Path[]::new)) {
                Files.deleteIfExists(file);
            }
        } else {
            Files.createDirectory(working);
        }
    }

    private static Path updateFile(Path working, String pCode) throws IOException {
        Map<String, String> changes = new HashMap<>();
        changes.put("PropertyCode=\"10735\"", "PropertyCode=\"" + pCode + "\"");
        changes.put("HotelID=\"10735\"", "HotelID=\"" + pCode + "\"");
        Path strDestFile = Paths.get(working.toString().replace("Hilton", "STRESSBOX").replace("KOAHW", pCode));
        FileHelper.updateFile(changes, working, strDestFile);
        return strDestFile;
    }
}

package com.sbn.utills;

import java.io.File;

import java.io.FileOutputStream;
import java.io.IOException;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

/**********************************
 * Created by Kirtesh Wani on 04-03-2015. for ExcelBasedFramework
 ***********************************/

public class ExcelReader {
    private Workbook workbook;
    private final String excelFilePath;

    public ExcelReader(String excelFilePath) throws IOException {
        workbook = WorkbookFactory.create(new File(excelFilePath));
        this.excelFilePath = excelFilePath;
    }

    public ExcelReader(String excelFilePath, String password) throws IOException {
        workbook = WorkbookFactory.create(new File(excelFilePath), password);
        this.excelFilePath = excelFilePath;
    }

    public static void createExcelFile(String fileName, String sheetName) throws IOException, InvalidFormatException {
        Workbook workbook;
        if (fileName.endsWith(".xlsx")) {
            workbook = new XSSFWorkbook(new File(fileName));
        } else {
            workbook = new HSSFWorkbook();
        }
        workbook.createSheet(sheetName);
        try (FileOutputStream outputStream = new FileOutputStream(fileName)) {
            workbook.write(outputStream);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void close() throws IOException {
        workbook.close();
    }

    public String getCellStringData(String SheetName, int RowNum, int ColNum) {
        try {
            Sheet sheet = workbook.getSheet(SheetName);
            Cell cell = sheet.getRow(RowNum).getCell(ColNum);
            switch (sheet.getRow(RowNum).getCell(ColNum).getCellType()) {
                case STRING:
                    return cell.getRichStringCellValue().getString();
                case NUMERIC:
                    if (DateUtil.isCellDateFormatted(cell)) {
                        return cell.getDateCellValue().toString();
                    } else {
                        return Double.toString(cell.getNumericCellValue());
                    }
                case BOOLEAN:
                    return Boolean.toString(cell.getBooleanCellValue());
                case FORMULA:
                    return cell.getCellFormula();
                case BLANK:
                    return "";
                default:
                    return cell.getStringCellValue();
            }
        } catch (Exception e) {
            return "";
        }
    }

    public void setCellStringValue(String value, String SheetName, int RowNum, int ColNum) throws IOException {
        Sheet sheet = workbook.getSheet(SheetName);
        if (sheet == null) {
            createSheet(SheetName);
        }
        Row row = sheet.getRow(RowNum);
        if (row == null) {
            row = sheet.createRow(RowNum);
        }
        Cell cell = row.getCell(ColNum);
        if (cell == null) {
            cell = row.createCell(ColNum);
        }
        cell.setCellValue(value);
    }

    public void saveWorkBook() throws IOException, InvalidFormatException {
        FileOutputStream outFile = new FileOutputStream(excelFilePath);
        workbook.write(outFile);
        outFile.close();
        ExcelReaderInIt();
    }

    public int getRowCount(String SheetName) {
        return workbook.getSheet(SheetName).getPhysicalNumberOfRows();
    }

    public int getColumnCount(String SheetName, int RowNum) {
        return workbook.getSheet(SheetName).getRow(RowNum).getPhysicalNumberOfCells();
    }

    public boolean createSheet(String SheetName) {
        try {
            // FileOutputStream output = new FileOutputStream(excelFilePath);
            workbook.createSheet(SheetName);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public boolean deleteSheet(String SheetName) {
        try {
            workbook.removeSheetAt(workbook.getSheetIndex(SheetName));
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    private void ExcelReaderInIt() throws IOException, InvalidFormatException {
        workbook = new XSSFWorkbook(new File(excelFilePath));
    }
}